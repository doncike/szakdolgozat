package nagy_daniel_szakdolgozat.view_entity;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Controller;

import nagy_daniel_szakdolgozat.entity.User;

@Controller
public class ViewMapper {
	
	private final ModelMapper modelMapper;

	public ViewMapper() {
		this.modelMapper = new ModelMapper();

		this.modelMapper.addMappings(new PropertyMap<UserView, User>() {
			@Override
			protected void configure() {
				map().setUsername(source.getUsername());
				map().setPassword(source.getPassword());
				map().setFirstName(source.getFirstName());
				map().setLastName(source.getLastName());
				map().setEmail(source.getEmail());
			}
		});
	}

	public User toUser(UserView userView) {
		return modelMapper.map(userView, User.class);
	}
}
