package nagy_daniel_szakdolgozat.service;

import nagy_daniel_szakdolgozat.entity.Animal;

import java.util.List;

public interface AnimalService {
    void saveAnimal(Animal animal);

    List<Animal> searchAnimal(String breed);
}
