package nagy_daniel_szakdolgozat.service;

import nagy_daniel_szakdolgozat.entity.User;

public interface UserService {
    void saveUser(User user);

    User findByUsername(String userName);

}
