package nagy_daniel_szakdolgozat.service;

import nagy_daniel_szakdolgozat.entity.Animal;
import nagy_daniel_szakdolgozat.entity.Shelter;
import nagy_daniel_szakdolgozat.repository.AnimalRepository;
import nagy_daniel_szakdolgozat.repository.ShelterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnimalServiceImpl implements AnimalService{

    @Autowired
    AnimalRepository animalRepository;
    @Autowired
    ShelterRepository shelterRepository;

    @Transactional
    @Override
    public void saveAnimal(Animal animal) {
        Shelter shelter = shelterRepository.findByName(animal.getShelterName());
        animal.setAnimalInShelter(shelter);
        animalRepository.save(animal);
    }

    @Override
    public List<Animal> searchAnimal(String search) {
        return animalRepository.findByBreedContaining(search);
    }

}
