package nagy_daniel_szakdolgozat.controller;

import nagy_daniel_szakdolgozat.entity.Animal;
import nagy_daniel_szakdolgozat.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SearchController {

    @Autowired
    AnimalService animalService;

    @GetMapping("/search")
    public List<Animal> searchAnimal(@RequestParam String search) {
        return animalService.searchAnimal(search);
    }
}
