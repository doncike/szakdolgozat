package nagy_daniel_szakdolgozat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nagy_daniel_szakdolgozat.service.UserService;
import nagy_daniel_szakdolgozat.view_entity.UserView;
import nagy_daniel_szakdolgozat.view_entity.ViewMapper;

@RestController
//@CrossOrigin
public class UserController {

	private final UserService userService;
	private final ViewMapper viewMapper;

	@Autowired
	public UserController(UserService userService, ViewMapper viewMapper) {
		this.viewMapper = viewMapper;
		this.userService = userService;
	}

	@PostMapping("/registration")
	public String registerUser(@RequestBody UserView userView) {
		userService.saveUser(viewMapper.toUser(userView));

		return "alma";
	}
	@GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
}
