package nagy_daniel_szakdolgozat.controller;

import nagy_daniel_szakdolgozat.entity.Animal;
import nagy_daniel_szakdolgozat.repository.AnimalRepository;
import nagy_daniel_szakdolgozat.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin
public class AnimalController {

    private final AnimalRepository animalRepository;
    private final AnimalService animalService;

    @Autowired
    public AnimalController(AnimalRepository animalRepository, AnimalService animalService) {
        this.animalRepository = animalRepository;
        this.animalService = animalService;
    }

    @GetMapping("/animals")
    public List<Animal> getAnimals() {
        return animalRepository.findAll();
    }

    @PostMapping("/addanimal")
    public Animal addAnimal(@RequestBody Animal animal) {
        animalService.saveAnimal(animal);
        return animal;
    }
}
