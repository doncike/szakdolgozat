package nagy_daniel_szakdolgozat.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "kötelező megadni")
	private String firstName;

	@NotEmpty(message = "kötelező megadni")
	private String lastName;

	@NotEmpty(message = "kötelező megadni")
	@Email(message = "Az email cím nem megfelelő formátumú")
	@Column(unique = true)
	private String email;

	@NotEmpty(message = "kötelező megadni")
	@Size(min = 7, message = "Legalább 7 karakter hosszú jelszó szükséges")
	private String password;

	@Column(unique = true)
	@NotEmpty(message = "kötelező megadni")
	@Size(min = 6, max = 30, message = "A felhasználónévnek 6-30 hosszúnak kell lennie")
	private String username;

	@ManyToMany
	private Set<Role> roles;

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}