package nagy_daniel_szakdolgozat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Shelter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long shelterId;
	@NotEmpty
	private String name;
	@NotNull
	private Integer addresNumber;
	@NotEmpty
	private String city;
	@NotEmpty
	private String street;
	private String email;
	private Integer phoneNumber;
	@JsonIgnore//<------- kérdés
	@OneToMany(mappedBy = "animalInShelter")
	private List<Animal> animals;

	public Shelter() {

	}

	public Long getId() {
		return shelterId;
	}

	public void setId(Long shelterId) {
		this.shelterId = shelterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAddresNumber() {
		return addresNumber;
	}

	public void setAddresNumber(Integer addresNumber) {
		this.addresNumber = addresNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}
}
