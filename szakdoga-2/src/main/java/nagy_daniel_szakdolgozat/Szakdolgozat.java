package nagy_daniel_szakdolgozat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class Szakdolgozat{

	public static void main(String[] args) {
		SpringApplication.run(Szakdolgozat.class, args);
	}

}
