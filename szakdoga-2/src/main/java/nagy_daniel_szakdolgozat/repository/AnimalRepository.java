package nagy_daniel_szakdolgozat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nagy_daniel_szakdolgozat.entity.Animal;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long>{
	
	List<Animal> findByBreedContaining(String breed);
}
