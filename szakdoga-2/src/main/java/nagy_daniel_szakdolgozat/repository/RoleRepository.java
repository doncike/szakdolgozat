package nagy_daniel_szakdolgozat.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import nagy_daniel_szakdolgozat.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	
	@Query("SELECT role FROM Role role WHERE role.name = 'USER'")
	Set<Role> addUserRole();

}
