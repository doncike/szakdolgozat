package com.example.demo.userController;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.Szakdoga2ApplicationTests;

import nagy_daniel_szakdolgozat.entity.User;

public class UserControllerTest extends Szakdoga2ApplicationTests{

	 @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	 
	 @Test
	   public void createProduct() throws Exception {
	      String uri = "/registration";
	      User user = new User();
	      user.setId((long) 2266);
	      user.setEmail("asf2");
	      String inputJson = super.mapToJson(user);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      System.out.println(status);
	      String content = mvcResult.getResponse().getContentAsString();
	      System.out.println(content);
	   }
}
