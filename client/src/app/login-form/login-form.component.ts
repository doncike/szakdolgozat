import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  model: LoginViewModel = {
    username: '',
    password: ''
  };

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  login(): void {
    let url = 'http://localhost:8080/login';
    this.http.post(url, this.model);
  }
}
export interface LoginViewModel {
  username: string;
  password: string;
}
